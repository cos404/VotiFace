import React from 'react';
import MainVote from './MainVote'
import RatioBar from './RatioBar'

const MainPoll = ({poll}) => {
  return (
    <div id="main">
      <h4 id="title">{poll.title}</h4>
      <div id="votes">
        <MainVote vote={poll.votes[0]} />
        <MainVote vote={poll.votes[1]} />
      </div>
      <RatioBar vote1={poll.votes[0].count} vote2={poll.votes[1].count} />
    </div>
  )
}

export default MainPoll;