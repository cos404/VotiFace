import React from 'react';
import MainPoll from './MainPoll'

const poll = {
  ok: 'true',
  _id: '5a651637e70dc21a2b7fed24',
  title: 'What is better?',
  created_at: "2018-02-05T16:14:20.371Z",
  votes:[
    {
      count: 74,
      _id: '5a7884224fe14313b0a0562a',
      poll_id: '5a7882dc075c311289310328',
      title:"Grecha",
      image:'https://polzavred.ru/wp-content/uploads/polza-grechki.jpg'
    },
    {
      count: 35,
      _id: '5a7884834fe14313b0a0562b',
      poll_id: '5a7882dc075c311289310328',
      title:"Ris",
      image:'http://image.etov.ua/storage/640x640/c/3/c/f/c3cfe7746d31e890b49b37e8fa43222c.png'
    }
  ]
};

const App = () => {
  return(
    <MainPoll poll={poll}/>
  )
}

export default App;