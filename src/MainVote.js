import React, {Component} from 'react';
import PropTypes from 'prop-types'
class MainVote extends Component {
  static propTypes = {
    vote: PropTypes.object.isRequired
  };

  render(){
    return (
      <div id={"vote-" + this.props.vote.id} className="vote">
        <img src={this.props.vote.image} alt="" />
        <a className="hvr-fade" onClick={this.alerts} data-poll-id={this.props.vote.poll_id} data-id={this.props.vote.id}>Vote</a>
      </div>
    );
  }
}

export default MainVote;