import React from 'react';
import PropTypes from 'prop-types'

const RatioBar = ({vote1, vote2}) => {
  return (
    <div id="ratio">
      <progress max={vote1 + vote2} value={vote1} />
    </div>
  );

}

export default RatioBar;